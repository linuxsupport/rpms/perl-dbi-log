#
# This SPEC file was automatically generated using the cpantorpm
# script.
#
#    Package:           perl-DBI-Log
#    Version:           0.08
#    cpantorpm version: 1.09
#    Date:              Thu Apr 29 2021
#    Command:
# /usr/local/bin/cpantorpm --packager djuarezg@cern.ch DBI\:Log
#

Name:           perl-DBI-Log
Version:        0.08
Release:        1%{?dist}
Summary:        Log all DBI queries
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/DBI-Log/
BuildArch:      noarch
Source0:        DBI-Log-%{version}.tar.gz

#
# Unfortunately, the automatic provides and requires do NOT always work (it
# was broken on the very first platform I worked on).  We'll get the list
# of provides and requires manually (using the RPM tools if they work, or
# by parsing the files otherwise) and manually specify them in this SPEC file.
#

Provides:       perl(DBI::Log) = 0.08
Requires:       perl >= 5.006
Requires:       perl(DBI)
Requires:       perl(strict)
Requires:       perl(warnings)
Requires:       perl-DBD-SQLite
BuildRequires:  perl >= 5.006
BuildRequires:  perl(DBI)
BuildRequires:  perl-DBD-SQLite
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
BuildRequires:  perl-Test-Simple
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
You can use this module to log all queries that are made with DBI. You just
need to include it in your script and it will work automatically. By
default, it will send output to STDERR, which is useful for command line
scripts and for CGI scripts since STDERR will appear in the error log.

%prep

rm -rf %{_builddir}/DBI-Log-%{version}
%setup -D -n DBI-Log-0.08
chmod -R u+w %{_builddir}/DBI-Log-%{version}

if [ -f pm_to_blib ]; then rm -f pm_to_blib; fi

%build

%{__perl} Makefile.PL OPTIMIZE="$RPM_OPT_FLAGS" INSTALLDIRS=site INSTALLSITEBIN=%{_bindir} INSTALLSITESCRIPT=%{_bindir} INSTALLSITEMAN1DIR=%{_mandir}/man1 INSTALLSITEMAN3DIR=%{_mandir}/man3 INSTALLSCRIPT=%{_bindir}
make %{?_smp_mflags}

#
# This is included here instead of in the 'check' section because
# older versions of rpmbuild (such as the one distributed with RHEL5)
# do not do 'check' by default.
#

if [ -z "$RPMBUILD_NOTESTS" ]; then
   make test
fi

%install

rm -rf $RPM_BUILD_ROOT
make pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -type f -name '*.bs' -size 0 -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;
%{_fixperms} $RPM_BUILD_ROOT/*

%clean

rm -rf $RPM_BUILD_ROOT

%files

%defattr(-,root,root,-)
%{perl_sitelib}/*
%{_mandir}/man3/*

%changelog
* Thu Apr 29 2021 djuarezg@cern.ch 0.08-1
- Generated using cpantorpm

